GUI For Nanite

https://bitbucket.org/hansharmon/nanite/

![Preview](docs/images/nanite_ui.png)

This is based on react-storm:  https://github.com/projectstorm

This also uses material-ui:  https://material-ui.com

import React from "react";

import createEngine, {
  DefaultNodeModel,
  DiagramModel,
} from "@projectstorm/react-diagrams";
import { CanvasWidget } from "@projectstorm/react-canvas-core";
import { DeleteItemsAction } from '@projectstorm/react-canvas-core';
import "./App.css";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SplitPane from 'react-split-pane';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { BooleanNodeModel } from './Boolean/BooleanNodeModel';
import { BooleanNodeFactory } from './Boolean/BooleanNodeFactory';
import { BooleanPortFactory } from './Boolean/BooleanPortFactory';

class App extends React.Component {

  render() {

    //1) setup the diagram engine
    var engine = createEngine({
      registerDefaultDeleteItemsAction: false
    });
    engine.getNodeFactories().registerFactory(new BooleanNodeFactory());
    engine.getPortFactories().registerFactory(new BooleanPortFactory());
    engine.getActionEventBus().registerAction(new DeleteItemsAction({keyCodes: [46]}));

    //2) setup the diagram model
    var model = new DiagramModel();
    engine.setModel(model);

    // Create the full UX here.
    return (
      <SplitPane split="horizontal" minSize={60} maxSize={60} >
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className="menuButton" color="inherit" aria-label="menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className="title">
              Nanite Builder
            </Typography>
          </Toolbar>
        </AppBar>
        <SplitPane split="horizontal" minSize={150} defaultSize="85%">
          <SplitPane split="vertical" minSize={150} defaultSize="150px">
            <div>
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>Edges</Typography>
              </AccordionSummary>
              <Divider />
              <AccordionDetails>
                <List>
                  <ListItem className="nodeListItem_Edge" draggable={true} onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'Boolean'); }} ><ListItemText primary="Boolean" /></ListItem>
                  <ListItem className="nodeListItem_Edge" draggable={true} onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'Integer'); }} ><ListItemText primary="Integer" /></ListItem>
                  <ListItem className="nodeListItem_Edge" draggable={true} onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'Float'); }} ><ListItemText primary="Float" /></ListItem>
                  <ListItem className="nodeListItem_Edge" draggable={true} onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'String'); }} ><ListItemText primary="String" /></ListItem>
                </List>
              </AccordionDetails>
            </Accordion>
            <Accordion>              
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>Math/Logic</Typography>
              </AccordionSummary>
              <Divider />
              <AccordionDetails>
                <List>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'And'); }}><ListItemText primary="And" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', 'Or'); }}><ListItemText primary="Or" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '+'); }}><ListItemText primary="+" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '-'); }}><ListItemText primary="-" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '*'); }}><ListItemText primary="*" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '/'); }}><ListItemText primary="/" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '>'); }}><ListItemText primary=">" /></ListItem>
                  <ListItem className="nodeListItem_Math" draggable={true}  onDragStart={(event) => { event.dataTransfer.setData('storm-diagram-node', '<'); }}><ListItemText primary="<" /></ListItem>
                </List>
              </AccordionDetails>
            </Accordion>
            </div>
            <div
              onDrop={(event) => {
                var node_type = event.dataTransfer.getData('storm-diagram-node');
                var new_node;
                switch (node_type) {
                    case 'Boolean':
                      new_node = new BooleanNodeModel(node_type,"lightgrey");
                      break;
                    case 'Integer':
                    case 'Float':
                    case 'String':
                      new_node = new DefaultNodeModel(node_type, "lightgrey");
                      new_node.addInPort("I");
                      new_node.addOutPort("Q");
                      break;
                  case 'And':
                  case 'Or':
                  case '+':
                  case '-':
                  case '*':
                  case '/':
                  case '>':
                  case '<':
                      new_node = new DefaultNodeModel("C = A " + node_type + " B", "lightblue");
                      // To Do Dynamic Input Ports
                      new_node.addInPort("A");
                      new_node.addInPort("B");
                      new_node.addOutPort("C");
                      break;
                  }
                if (new_node != null) {
                  var point = engine.getRelativeMousePoint(event);
                  new_node.setPosition(point);
                  model.addNode(new_node);
                  engine.setModel(model); // This refreshes the canvas, not sure if the best method, but it's working.
                }
              }}
              onDragOver={(event) => {
                event.preventDefault();
              }}
              >
            <SplitPane split="vertical" minSize={150} defaultSize="150px" primary="second">
              <div>
                <CanvasWidget engine={engine} className="canvas diagram" />
              </div>
              <div>
                Properties
              </div>
              </SplitPane>
            </div>
          </SplitPane>

          <TextField
            fullWidth={true}
            id="storm_json"
            label="Diagram Json"
            multiline
            rows={4}
            defaultValue=""
            variant="outlined"
          />
        </SplitPane>
      </SplitPane>
    );
  }
}

export default App;

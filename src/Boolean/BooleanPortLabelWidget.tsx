import * as React from 'react';
import { DiagramEngine, PortWidget } from '@projectstorm/react-diagrams-core';
import { BooleanPortModel } from './BooleanPortModel';
import styled from '@emotion/styled';

export interface BooleanPortLabelProps {
	port: BooleanPortModel;
	engine: DiagramEngine;
}

export const MyBooleanPortLabel = styled.div`
    display: flex;
    margin-top: 1px;
    align-items: center;
`;

export const BooleanLabel = styled.div`
    padding: 0 5px;
    flex-grow: 1;
`;

export const BooleanPort = styled.div`
    width: 15px;
    height: 15px;
    background: rgba(255, 255, 255, 0.1);

    &:hover {
        background: rgb(192, 255, 0);
    }
`;

export class BooleanPortLabel extends React.Component<BooleanPortLabelProps> {
	render() {
		const port = (
			<PortWidget engine={this.props.engine} port={this.props.port}>
				<BooleanPort />
			</PortWidget>
		);
		const label = <BooleanLabel>{this.props.port.getOptions().label}</BooleanLabel>;

		return (
			<MyBooleanPortLabel>
				{this.props.port.getOptions().in ? port : label}
				{this.props.port.getOptions().in ? label : port}
			</MyBooleanPortLabel>
		);
	}
}
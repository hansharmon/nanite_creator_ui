import * as React from 'react';
import { BooleanNodeModel } from './BooleanNodeModel';
import { BooleanWidget } from './BooleanWidget';
import { AbstractReactFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';
import { GenerateModelEvent} from "@projectstorm/react-canvas-core";

export class BooleanNodeFactory extends AbstractReactFactory<BooleanNodeModel, DiagramEngine> {
	constructor() {
		super('Boolean');
	}

	generateReactWidget(event: { model: BooleanNodeModel; }): JSX.Element {
		return <BooleanWidget engine={this.engine} node={event.model} />;
	}

	generateModel(event: GenerateModelEvent): BooleanNodeModel {
		return new BooleanNodeModel();
	}
}


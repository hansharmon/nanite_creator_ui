import { BooleanPortModel } from './BooleanPortModel';
import { AbstractModelFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';

export class BooleanPortFactory extends AbstractModelFactory<BooleanPortModel, DiagramEngine> {
	constructor() {
		super('boolean');
	}

	generateModel(): BooleanPortModel {
		return new BooleanPortModel({
			name: 'unknown'
		});
	}
}

// Boolean Model
// Attempt to put a checkbox on to a boolean edge box.
// import { NodeModel, NodeModelGenerics, PortModelAlignment } from '@projectstorm/react-diagrams';
import * as React from 'react';
import * as _ from 'lodash';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';
import { BooleanNodeModel } from './BooleanNodeModel';
import { BooleanPortLabel } from './BooleanPortLabelWidget';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import styled from '@emotion/styled';

	export const BooleanNode = styled.div<{ background: string; selected: boolean }>`
		background-color: ${(p) => p.background};
		border-radius: 5px;
		font-family: sans-serif;
		color: white;
		border: solid 2px black;
		overflow: visible;
		font-size: 11px;
		border: solid 2px ${(p) => (p.selected ? 'rgb(0,192,255)' : 'black')};
	`;

	export const BooleanTitle = styled.div`
		background: rgba(0, 0, 0, 0.3);
		display: flex;
		white-space: nowrap;
		justify-items: center;
	`;

	export const BooleanTitleName = styled.div`
		flex-grow: 1;
		padding: 5px 5px;
	`;

	export const BooleanPorts = styled.div`
		display: flex;
		background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2));
	`;

	export const BooleanPortsContainer = styled.div`
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		&:first-of-type {
			margin-right: 10px;
		}
		&:only-child {
			margin-right: 0px;
		}
	`;


	export const BooleanControlsContainer = styled.div`
		flex-grow: 1;
		display: flow;
		padding: 0px 10px;
		justify-items: center;
		white-space: nowrap;
		background: rgba(0, 0, 0, 0.3);
	`;

export interface BooleanNodeProps {
	node: BooleanNodeModel;
	engine: DiagramEngine;
}



export class BooleanWidget  extends React.Component<BooleanNodeProps> {

	generatePort = (port) => {
		return <BooleanPortLabel engine={this.props.engine} port={port} key={port.getID()} />;
	};

	handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		event.stopPropagation();
	  };

	render() {
		return(
        <BooleanNode
			data-default-node-name={this.props.node.getOptions().name}
			selected={this.props.node.isSelected()}
			background={this.props.node.getOptions().color}>
			<BooleanTitle>
				<TextField hiddenLabel size="small" defaultValue={this.props.node.getOptions().name} margin="none" onChange={this.handleChange} />
			</BooleanTitle>			
			<BooleanControlsContainer>
				F<Switch color="info" size='small'/>T
			</BooleanControlsContainer>
			<BooleanPorts>
				<BooleanPortsContainer>{_.map(this.props.node.getInPorts(), this.generatePort)}</BooleanPortsContainer>
				<BooleanPortsContainer>{_.map(this.props.node.getOutPorts(), this.generatePort)}</BooleanPortsContainer>
			</BooleanPorts>
    	</BooleanNode>    
    );
	}
}


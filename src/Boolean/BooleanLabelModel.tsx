import { DiagramEngine, LabelModel, LabelModelGenerics, LabelModelOptions } from '@projectstorm/react-diagrams-core';
import { DeserializeEvent } from '@projectstorm/react-canvas-core';

export interface BooleanLabelModelOptions extends LabelModelOptions {
	label?: string;
}

export interface BooleanLabelModelGenerics extends LabelModelGenerics {
	OPTIONS: BooleanLabelModelOptions;
}

export class BooleanLabelModel extends LabelModel<BooleanLabelModelGenerics> {
	constructor(options: BooleanLabelModelOptions = {}) {
		super({
			offsetY: options.offsetY == null ? -23 : options.offsetY,
			type: 'default',
			...options
		});
	}

	setLabel(label: string) {
		this.options.label = label;
	}

	deserialize(event: DeserializeEvent<this>) {
		super.deserialize(event);
		this.options.label = event.data.label;
	}

	serialize() {
		return {
			...super.serialize(),
			label: this.options.label
		};
	}
}
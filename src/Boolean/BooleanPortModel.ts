import {
	LinkModel,
	PortModel,
	PortModelAlignment,
	PortModelGenerics,
	PortModelOptions
} from '@projectstorm/react-diagrams-core';
//import { DefaultLinkModel } from '@projectstorm/react-diagrams-defaults';
import { PathFindingLinkModel } from '@projectstorm/react-diagrams-routing';
import { AbstractModelFactory, DeserializeEvent } from '@projectstorm/react-canvas-core';

export interface BooleanPortModelOptions extends PortModelOptions {
	label?: string;
	in?: boolean;
	type?: string;
}

export interface BooleanPortModelGenerics extends PortModelGenerics {
	OPTIONS: BooleanPortModelOptions;
}

export class BooleanPortModel extends PortModel<BooleanPortModelGenerics> {
	constructor(isIn: boolean, name?: string, label?: string);
	constructor(options: BooleanPortModelOptions);
	constructor(options: BooleanPortModelOptions | boolean, name?: string, label?: string) {
		if (!!name) {
			options = {
				in: !!options,
				name: name,
				label: label
			};
		}
		options = options as BooleanPortModelOptions;
		super({
			label: options.label || options.name,
			alignment: options.in ? PortModelAlignment.LEFT : PortModelAlignment.RIGHT,
			type: 'boolean',
			...options
		});
	}

	deserialize(event: DeserializeEvent<this>) {
		super.deserialize(event);
		this.options.in = event.data.in;
		this.options.label = event.data.label;
	}

	serialize() {
		return {
			...super.serialize(),
			in: this.options.in,
			label: this.options.label
		};
	}

	link<T extends LinkModel>(port: PortModel, factory?: AbstractModelFactory<T>): T {
		let link = this.createLinkModel(factory);
		link.setSourcePort(this);
		link.setTargetPort(port);
		return link as T;
	}

	canLinkToPort(port: PortModel): boolean {
		if (port instanceof BooleanPortModel) {
			return this.options.in !== port.getOptions().in;
		}
		return false;
	}

	createLinkModel(factory?: AbstractModelFactory<LinkModel>): LinkModel {
		let link = super.createLinkModel();
		if (!link && factory) {
			return factory.generateModel({});
		}
		return link || new PathFindingLinkModel();
	}
}